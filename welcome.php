<?php
// Otworzenie sesji
session_start();

// Sprawdzenie, czy użytkownik jest zalogowany
if(!isset($_SESSION['username'])) {
    header("Location: index.php");
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Witaj!</title>
</head>
<body>
	<h1>Witaj, <?php echo $_SESSION['username']; ?>!</h1>
	<p>Logowanie powiodło się. Brawo to Twój prosty system logowania w PHP</p>
	<p><a href="logout.php">Wyloguj mnie</a></p>
</body>
</html>