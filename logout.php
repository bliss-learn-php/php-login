<?php
session_start();

// Usunięcie danych sesji
session_unset();
session_destroy();
// Przekierowanie użytkownika na stronę logowania
header("Location: index.php");
exit;