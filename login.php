<?php
// Otworzenie sesji
session_start();

// Przykładowe dane użytkowników (w praktyce warto je przechowywać w bazie danych oraz dodatkowo zaszyfrować hasło)
// dla uproszczenia, wrzućmy je do tablicy
$users = array(
    "user1" => "password1",
    "user2" => "password2",
    "user3" => "password3"
);

// Sprawdzenie, czy użytkownik przesłał formularz
if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Sprawdzenie, czy dane użytkownika są poprawne
    if (isset($users[$username]) && $users[$username] == $password) {
        $_SESSION['username'] = $username;
        header("Location: welcome.php");
        exit;
    } else {
        $error = "Nieprawidłowa nazwa użytkownika lub hasło.";
    }
}
?>

<!-- Ponownie wyświetlamy formularz -->
<!DOCTYPE html>
<html>
<head>
	<title>Proste logowanie</title>
</head>
<body>
	<h1>Proste logowanie</h1>
    <!-- Jeżeli nie mamy dostępu, wyświetlamy błąd -->
	<?php
		if(isset($error)) {
			echo '<p style="color: red;">' . $error . '</p>';
		}
	?>
	<form method="POST" action="login.php">
		<label for="username">Nazwa użytkownika:</label>
		<input type="text" name="username" id="username"><br>
		<label for="password">Hasło:</label>
		<input type="password" name="password" id="password"><br>
		<input type="submit" name="submit" value="Zaloguj się">
	</form>
</body>
</html>